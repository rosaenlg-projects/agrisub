const fs = require('fs');

function loadFromDb() {
  return JSON.parse(fs.readFileSync('data/db/agrisubdataraw.json', 'utf8'));
}
module.exports = {
  loadFromDb
}

