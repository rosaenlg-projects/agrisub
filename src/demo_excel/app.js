var dataprep = require('../dataprep.js');

function getDataFromWorkbook(workbook) {
  var sheetName = workbook.SheetNames[0];
  var sheet = workbook.Sheets[sheetName];

  const cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];

  // read col names
  var colNames = [];
  for (var i=0; i<cols.length; i++) {
    var val = sheet[cols[i]+1].v;
    colNames.push(val);
  }

  var data = [];
  var line = 2;
  while (true) {
    if (!sheet['A'+line]) {
      break;
    }

    var dataLine = {};
    data.push(dataLine);
    for (var i=0; i<cols.length; i++) {
      var val = sheet[cols[i]+line].v;
      dataLine[colNames[i]] = val;
      //console.log(`${colNames[i]} => ${val}`);
    }

    line++;
  }
  
  return data;

}

var preparedData;

function render() {
  let rendered = template({
    util: new rosaenlg_fr_FR.NlgLib({language: 'fr_FR'}),
    data: preparedData
  });

  document.querySelector('#result').innerHTML = rendered;

}

function handleDrop(e) {
  e.stopPropagation(); e.preventDefault();
  //var files = e.dataTransfer.files;
  var files = document.querySelector('#excelFile').files;
  var f = files[0];
  var reader = new FileReader();
  reader.onload = function(e) {
    var data = new Uint8Array(e.target.result);
    var workbook = XLSX.read(data, {type: 'array'});

    //console.log(workbook);

    var workbookData = getDataFromWorkbook(workbook);
    preparedData = dataprep.getDataForTextsWithCluster(workbookData);

    document.querySelector('#refreshButton').style.display = "block";

    render();
  };
  reader.readAsArrayBuffer(f);
}

window.onload = function() {
  let input = document.querySelector('#excelFile');  
  input.addEventListener('change', handleDrop, false);

  let refreshButton = document.querySelector('#refreshButton')
  refreshButton.addEventListener('click', refresh);
  refreshButton.style.display = "none";

};

function refresh() {
  render();
}
