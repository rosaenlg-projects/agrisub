var levenshtein = require('fast-levenshtein');
var cluster = require('hierarchical-clustering');
var stats = require("stats-lite");

function rebuildObjects(agrisubData) {

  var agrisubObjects = [];

  var current = null;

  for (var i=0; i<agrisubData.length; i++) {
    var data = agrisubData[i];

    function addDetailHelper() {
      detail = {};
      detail.id = data.id;
      detail.date_convention = new Date(data.date_convention); // car apparemment n'est pas remonté comme une date
      detail.objet_convention = data.objet_convention;
      detail.montant = data.montant_total;
      current.detail.push(detail);
    }

    if (current!=null && data.identification_beneficiaire==current.identification_beneficiaire) {
      // c'est la suite de l'objet
      addDetailHelper();
    } else {
      // changement d'objet, ou alors le tout 1er
      if (current!=null) { // null si c'est le 1er
        agrisubObjects.push(current);
      }
      current = {};
      current.identification_beneficiaire = data.identification_beneficiaire;
      current.nom_beneficiaire = data.nom_beneficiaire;
      current.total = data.total;

      current.detail = [];

      addDetailHelper();
    }
  }
  agrisubObjects.push(current); // ne pas oublier le petit dernier
  //console.log(agrisubObjects);
  
  return agrisubObjects;
}


function groupSameLabels(clusteredObjects) {

  for (var i=0; i<clusteredObjects.length; i++) {
    var clusters = clusteredObjects[i].clusters;

    for (var j=0; j<clusters.length; j++) {
      var cluster = clusters[j];
      
      sameLabels = [];
      for (var k=0; k<cluster.elts.length; k++) {
        var objet_convention = cluster.elts[k].objet_convention;
        // console.log(objet_convention);
        if ( sameLabels.indexOf(objet_convention)==-1 ) {
          sameLabels.push(objet_convention);
        }
      }
      // console.log(sameLabels);

      cluster.sameLabels = sameLabels;
      if (sameLabels.length!=cluster.length) {
        //console.log( "grouped labels: " + sameLabels );
      }

    }


  }

}

function buildClusters(allObjects) {

  function getClusters(detailObjects) {
    function distance(a, b) {
      var sa = a.objet_convention;
      var sb = b.objet_convention;
  
      if ( sa.indexOf(sb)!=-1 || sb.indexOf(sa)!=-1 ) {
        return 0;
      }
      var dist = levenshtein.get(sa, sb);
      // console.log(`[${a}] [${b}]: ${dist}`);
      return dist;
    }
  
    // Single-linkage clustering
    function linkage(distances) {
      return Math.min.apply(null, distances);
    }
  
    var levels = cluster({
      input: detailObjects,
      distance: distance,
      linkage: linkage,
      //minClusters: 2, // only want two clusters
    });
  
    const threshold = 5;
  
    var level;
    for (var j=levels.length-1; j>=0; j--) { // parcours sens inverse
      if (levels[j].linkage < threshold || levels[j].linkage==null) {
        level = levels[j];
        break;
      }
    }
    //console.log(level.clusters);
    
    var finalClusters = level.clusters.map(function (cluster) {
      return cluster.map(function (index) {
        return detailObjects[index];
      });
    });
  
    return finalClusters;  
  }

  function enrichClusters(clusters) {
    for (var i=0; i<clusters.length; i++) {
      var c = clusters[i];
      var total = 0;
      for (var j=0; j<c.length; j++) {
        total += c[j].montant;
      }
      // console.log(total);
      
      var newCluster = {};
      newCluster.total_cluster = total;
      newCluster.elts = c;

      clusters[i] = newCluster;
      
  
    }
  
    //console.log(finalClusters);
    
  }
  
  for (var i=0; i<allObjects.length; i++) {
    allObjects[i].clusters = getClusters(allObjects[i].detail);
    enrichClusters(allObjects[i].clusters);
    allObjects[i].detail = null; // plus besoin
  }

}


function groupByCentiles(agrisubObjects, centiles, centilesLabels) {

  var totals = [];
  for (var i=0; i<agrisubObjects.length; i++) {
    totals.push( agrisubObjects[i].total );
  }

  var centilesVals = [];
  for (var i=0; i<centiles.length; i++) {
    centilesVals[i] = stats.percentile(totals, centiles[i]);
  }
  // console.log( centilesVals );

  //console.log("# valeurs : " + agrisubObjects.length);

  var data = [];
  var taken = [];
  for (var i=0; i<centilesLabels.length; i++) {
    var centile = {};
    centile.label = centilesLabels[i];
    centile.val = centilesVals[i];
    centile.subs = [];
    for (var j=0; j<agrisubObjects.length; j++) {
      if ( taken.indexOf(j)==-1 &&  ( agrisubObjects[j].total < centile.val || centile.val == null ) ) {
        centile.subs.push(agrisubObjects[j]);
        taken.push(j);
      }
    }
    data.push(centile);
    //console.log(`dans le centile < ${centile.val} : ${centile.subs.length}`);
  }
  data.reverse(); // du plus gros au plus petit

  return data;
}

function getDataForTextsWithCluster(agrisubData) {
  let agrisubObjects = rebuildObjects(agrisubData);

  buildClusters(agrisubObjects);
  
  groupSameLabels(agrisubObjects);

  return groupByCentiles(
    agrisubObjects,
    [0.1, 0.3, 0.7, 0.9],
    ["les 10% plus petits", "les petits", "les moyens", "les plutôt gros", "les 10% plus gros"] // 1 label de plus
  );

}

function getDataForTextsNoCluster(agrisubData) {

  let agrisubObjects = rebuildObjects(agrisubData);

  buildClusters(agrisubObjects);
  
  groupSameLabels(agrisubObjects);

  return agrisubObjects;
}

module.exports = {
  getDataForTextsNoCluster,
  getDataForTextsWithCluster
};
