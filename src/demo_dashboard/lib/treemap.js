
var shortName = require('./shortnames');
var accessors = require('./accessors');

function renderTreeMap() {

  function getDataWithChildren(rawData) {
    let nestedTmp = d3.nest()
      .key(function(d) { return d.identification_beneficiaire; })
      .entries(rawData);
    //console.log(nestedTmp);

    let data = {};
    data.data = {name: "subventions agricoles"};
    data.children = [];

    Array.prototype.forEach.call(nestedTmp, function(rawOrganisme) {
      let cleanOrganisme = {};
      data.children.push(cleanOrganisme);

      cleanOrganisme.data = null;
      cleanOrganisme.children = [];

      Array.prototype.forEach.call(rawOrganisme.values, function(rawSubvention) {
        if (cleanOrganisme.data == null) {
          cleanOrganisme.data = {
            'identification_beneficiaire': rawSubvention.identification_beneficiaire,
            'nom_beneficiaire': rawSubvention.nom_beneficiaire      
          };
        }
        let cleanSubvention = {
          'id': rawSubvention.id,
          'date_convention': rawSubvention.date_convention,
          'objet_convention': rawSubvention.objet_convention,
          'montant_total': rawSubvention.montant_total
        };
        cleanOrganisme.children.push(cleanSubvention);
      });

    });

    return data;
  }

  function render(data) {

    // set the dimensions and margins of the graph
    var margin = {top: 10, right: 10, bottom: 10, left: 10},
      width = 800 - margin.left - margin.right,
      height = 800 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    var svg = d3.select("#my_dataviz")
    .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    //console.log(data);

    // Give the data to this cluster layout:
    var root = d3.hierarchy(data).sum(function(d){ return d.montant_total}) // Here the size of each leave is given in the 'value' field in input data

    //console.log(root);

    // Then d3.treemap computes the position of each element of the hierarchy
    d3.treemap()
      .size([width, height])
      .paddingTop(28)
      .paddingRight(7)
      .paddingInner(3)      // Padding between each rectangle
      //.paddingOuter(6)
      //.padding(20)
      (root)

    // prepare a color scale
    var color = d3.scaleOrdinal(d3.schemeAccent)
      .range(d3.schemeCategory10)


    var maxSub = d3.max(root.leaves(), (elt) => {
      //console.log(elt);
      return elt.data.montant_total;
    });
    //console.log(maxSub);

    // And a opacity scale
    var opacity = d3.scaleLinear()
      .domain([0, maxSub])
      .range([.5,1])

    // use this information to add rectangles:
    svg
      .selectAll("rect")
      .data(root.leaves())
      .enter()
      .append("rect")
        .attr('x', function (d) { return d.x0; })
        .attr('y', function (d) { return d.y0; })
        .attr('width', function (d) { return d.x1 - d.x0; })
        .attr('height', function (d) { return d.y1 - d.y0; })
        .attr("id", function(d){ return d.data.id; })
        .style("stroke", "black")
        .style("fill", function(d){
          // couleur du détail en fonction de l'organisme. seule l'opacité change.
          let organismeName = d.parent.data.data.identification_beneficiaire;
          // console.log(organismeName);
          return color(organismeName);
        })
        .style("opacity", function(d){ return opacity(d.data.montant_total)})
        .on("click", function(d) {
          //console.log(`click on one specific sub: ${this.id}`);
          renderBlockDetail(this.id);
        })
        .attr("class", "clickable")
    
    const formatter = new Intl.NumberFormat('fr-FR', {
      style: 'decimal',
      maximumFractionDigits: 0
    })    

    // add the value text labels
    svg
      .selectAll("vals")
      .data(root.leaves())
      .enter()
      .append("text")
        .attr("x", function(d){ return d.x0+5})    // +10 to adjust position (more right)
        .attr("y", function(d){ return d.y0 + (d.y1-d.y0)/2 })
        .text(function(d){ return `${formatter.format( d.data.montant_total/1000 )} k€` })
        .each(getSize)
        .attr("font-size", function(d) { return d.scale + "px"; })
        .attr("fill", "white")
        .attr("id", function(d){ return d.data.id; })
        .on("click", function(d) {
          //console.log(`click on one specific sub (text): ${this.id}`);
          renderBlockDetail(this.id);
        })
        .attr("class", "clickable")
      
    function getSize(d) {
      var bbox = this.getBBox(),
        scale = Math.min( (d.x1-d.x0)/bbox.width, (d.y1-d.y0)/bbox.height) * 10;
      d.scale = scale;

      //console.log(`w:${w} h:${h} ${d.data.name} => ${scale}`);
    }

    function renderBlockText(id) {
      var dataElt = accessors.getDataOrganismeForText(id); // dataById[id];
      // console.log(dataElt);
      dataElt.level = 'one';
      let rendered = template({
        util: new rosaenlg_fr_FR.NlgLib({language: 'fr_FR'}),
        data: dataElt
      });
      //console.log(rendered);
      d3.select('#texteGenere').html(rendered);
    }

    function renderBlockDetail(id) {
      //console.log(getDataSubventionForText(id));
      var dataElt = accessors.getDataSubventionForText(id);
      dataElt.level = 'detail';
      let rendered = template({
        util: new rosaenlg_fr_FR.NlgLib({language: 'fr_FR'}),
        data: dataElt
      });
      //console.log(rendered);
      d3.select('#texteGenere').html(rendered);
    }

    // Add title for the each block
    svg
      .selectAll("titles")
      .data(root.descendants().filter(function(d){return d.depth==1}))
      .enter()
      .append("text")
        .attr("x", function(d){ return d.x0})
        .attr("y", function(d){ return d.y0+21})
        .text(function(d){
          //console.log(d.data.data.nom_beneficiaire);
          return shortName.getShortName(d.data.data.nom_beneficiaire);
        })
        .each(getSize)
        .attr("font-weight", "bold")
        .attr("font-size", function(d) { return Math.min(d.scale, 20) + "px"; })
        .attr("fill",  function(d){ return color(d.data.data.identification_beneficiaire)} )
        .attr("class", "clickable")
        .attr("id", function(d){ return d.data.data.identification_beneficiaire; })
        .on("click", function() {
          //console.log(this);
          //console.log(`click on title: ${this.id}`);
          renderBlockText(this.id);
        });


      
    // Add general title
    svg
      .append("text")
        .attr("x", 0)
        .attr("y", 14)    // +20 to adjust position (lower)
        .text("Subventions Agricoles")
        .attr("font-size", "19px")
        .attr("fill",  "grey" )
  
  }
  
  d3.json("agrisubdataraw.json").then(function(rawData) {
    
    accessors.setRawDataGlobal(rawData);

    let data = getDataWithChildren(rawData);
    //console.log(data);

    render(data);
    
  });

}

module.exports = {
  renderTreeMap,
}