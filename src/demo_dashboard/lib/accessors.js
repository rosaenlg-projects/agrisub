let _rawDataGlobal;
function setRawDataGlobal(rawDataGlobal) {
  _rawDataGlobal = rawDataGlobal;
}

let _dataForTextsGlobal;
function initDataForTexts() {
  d3.json("agrisubdata.json").then(function(data) {
    _dataForTextsGlobal = data;
  });
}



function getDataOrganismeForText(idOrganisme) {
  for (let i=0; i<_dataForTextsGlobal.length; i++) {
    if (_dataForTextsGlobal[i].identification_beneficiaire==idOrganisme) {
      return _dataForTextsGlobal[i];
    }
  }
  return null;
}


function getDataSubventionForText(idSubvention) {
  let dataSubvention;
  for (var i=0; i<_rawDataGlobal.length; i++) {
    if (_rawDataGlobal[i].id==idSubvention) {
      dataSubvention = _rawDataGlobal[i];
      break;
    }
  }

  // à harmoniser
  dataSubvention.montant = dataSubvention.montant_total;

  let dataOrganisme = getDataOrganismeForText(dataSubvention.identification_beneficiaire);

  return {
    'detail': dataSubvention,
    organisme: {
      'identification_beneficiaire': dataOrganisme.identification_beneficiaire,
      'nom_beneficiaire': dataOrganisme.nom_beneficiaire,
      'total': dataOrganisme.total
    }
  };
}

module.exports = {
  getDataOrganismeForText,
  getDataSubventionForText,
  setRawDataGlobal,
  initDataForTexts,
}
