var treemap = require('./lib/treemap.js');
var accessors = require('./lib/accessors');


window.onload = function() {
  treemap.renderTreeMap();
  accessors.initDataForTexts();
};
