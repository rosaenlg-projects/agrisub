const fs = require('fs');
const rosaenlg = require('rosaenlg');
const packager = require('rosaenlg-packager');


// compile RosaeNLG templates

function templates(cb) {
  const compiled = packager.compileTemplateToJsString('src/demo_dashboard/entry.pug', 'fr_FR', null, rosaenlg, true, false);
  fs.writeFile('dist/demo_pbi/agrisub.js', compiled, 'utf8', cb);
}


exports.all = templates;

