const { src, dest, parallel, series } = require('gulp');

const rename = require('gulp-rename');
const awspublish = require("gulp-awspublish");
const merge = require("merge-stream");

// publish S3

const baseFolder = 'demos';

function publishS3(folder) {

  var publisher = awspublish.create({
    "params": {
      "Bucket": "rosaenlg.org"
    }
  });

  var gzip = src([`dist/${folder}/*.js`])
    .pipe(rename(function (path) {
      path.dirname = `${baseFolder}/${folder}/` + path.dirname;
    }))
    .pipe(awspublish.gzip());

  var plain = src([`dist/${folder}/*`, `!dist/${folder}/*.js`]);

  return merge(gzip, plain)
    .pipe(rename(function (path) {
      path.dirname = `${baseFolder}/${folder}/` + path.dirname;
    }))
    .pipe(publisher.publish())
    .pipe(awspublish.reporter());

}

function static() {

  var publisher = awspublish.create({
    "params": {
      "Bucket": "rosaenlg.org"
    }
  });

  return src([`dist/*`])
    .pipe(rename(function (path) {
      path.dirname = `${baseFolder}/` + path.dirname;
    }))
    .pipe(publisher.publish())
    .pipe(awspublish.reporter());

}


function demoExcel() {
  return publishS3('demo_excel');
}
function demoBatch() {
  return publishS3('demo_batch');
}
function demoDashboard() {
  return publishS3('demo_dashboard');
}

exports.all = series(demoExcel, demoBatch, demoDashboard, static);
