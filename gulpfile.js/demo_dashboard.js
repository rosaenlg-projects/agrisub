const { src, dest, parallel, series } = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var log = require('gulplog');
const browserify = require('browserify');
const fs = require('fs');
const rosaenlg = require('rosaenlg');
const loadFromDb = require('../src/loadFromDb').loadFromDb;
const getDataForTextsNoCluster = require('../src/dataprep').getDataForTextsNoCluster;
const packager = require('rosaenlg-packager');

const rosaeNLGVersion = "1.14.0";

// copy

function copyHtml() {
  return src('src/demo_dashboard/demo_dashboard.html')
    .pipe(dest('dist/demo_dashboard/'));
}
function css() {
  return src('src/demo_dashboard/*.css')
    .pipe(dest('dist/demo_dashboard/'));
}
function copyJsLibs() {
  return src([
    'node_modules/d3/dist/d3.min.js', 
    `node_modules/rosaenlg/dist/rollup/rosaenlg_tiny_fr_FR_${rosaeNLGVersion}.js`
    ])
    .pipe(dest('dist/demo_dashboard/'));
}

// js
function js() {
  // set up the browserify instance on a task basis
  var b = browserify({
    entries: 'src/demo_dashboard/app.js',
    //debug: true,
    // defining transforms here will avoid crashing your stream
    // transform: [reactify]
  });

  return b.bundle()
    .pipe(source('app.min.js'))
    .pipe(buffer())
    //.pipe(sourcemaps.init({loadMaps: true}))
        // Add transformation tasks to the pipeline here.
        //.pipe(uglify())
        .on('error', log.error)
    //.pipe(sourcemaps.write('./'))
    .pipe(dest('./dist/demo_dashboard'));
}


// data

function genDataForTexts(cb) {
  fs.writeFileSync('dist/demo_dashboard/agrisubdata.json', JSON.stringify(
    getDataForTextsNoCluster(loadFromDb())
  ));
  cb();
}
function genDataForViz(cb) {
  fs.writeFileSync('dist/demo_dashboard/agrisubdataraw.json', JSON.stringify(
    loadFromDb()
  ));
  cb();
}


// compile RosaeNLG templates

function templates(cb) {
  const compiled = packager.compileTemplateToJsString('src/demo_dashboard/entry.pug', 'fr_FR', null, rosaenlg);
  fs.writeFile('dist/demo_dashboard/templates.js', compiled, 'utf8', cb);
}


exports.templates = templates;
exports.js = js;
exports.data = parallel(genDataForTexts, genDataForViz);
exports.copy = parallel(copyHtml, css, copyJsLibs, js);

exports.all = series(exports.templates, exports.data, exports.copy);
