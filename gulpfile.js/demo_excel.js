const { src, dest, parallel, series } = require('gulp');
const browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var log = require('gulplog');
//var uglify = require('gulp-uglify');
//var sourcemaps = require('gulp-sourcemaps');
//const concat = require('gulp-concat');
const fs = require('fs');
const packager = require('rosaenlg-packager');
const rosaenlg = require('rosaenlg');

const rosaeNLGVersion = "1.14.0";

// copy

function copyHtml() {
  return src('src/demo_excel/demo_excel.html')
    .pipe(dest('dist/demo_excel'));
}
function copyExcel() {
  return src('src/demo_excel/agribub_excel_demo.xlsx')
    .pipe(dest('dist/demo_excel'));
}
function copyCss() {
  return src('src/demo_excel/demo_excel.css')
    .pipe(dest('dist/demo_excel'));
}
function copyJsLibs() {
  return src([
    'node_modules/xlsx/dist/xlsx.full.min.js', 
    `node_modules/rosaenlg/dist/rollup/rosaenlg_tiny_fr_FR_${rosaeNLGVersion}.js`
    ])
    .pipe(dest('dist/demo_excel'));
}

// js
function js() {
  // set up the browserify instance on a task basis
  var b = browserify({
    entries: 'src/demo_excel/app.js',
    //debug: true,
    // defining transforms here will avoid crashing your stream
    // transform: [reactify]
  });

  return b.bundle()
    .pipe(source('app.min.js'))
    .pipe(buffer())
    //.pipe(sourcemaps.init({loadMaps: true}))
        // Add transformation tasks to the pipeline here.
        //.pipe(uglify())
        .on('error', log.error)
    //.pipe(sourcemaps.write('./'))
    .pipe(dest('./dist/demo_excel'));
}


// compile RosaeNLG templates

function templates(cb) {
  const compiled = packager.compileTemplateToJsString('src/demo_excel/demo_excel.pug', 'fr_FR', null, rosaenlg);
  fs.writeFile('dist/demo_excel/templates_excel.js', compiled, 'utf8', cb);
}


copy = parallel(copyHtml, copyCss, copyJsLibs);
exports.all = series(templates, copy, js, copyExcel);
