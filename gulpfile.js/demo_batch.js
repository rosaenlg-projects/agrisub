const { src, dest, parallel, series } = require('gulp');
const fs = require('fs');
const rosaenlg = require('rosaenlg');

const loadFromDb = require('../src/loadFromDb').loadFromDb;
const getDataForTextsWithCluster = require('../src/dataprep').getDataForTextsWithCluster;

function css() {
  return src('src/demo_batch/*.css')
    .pipe(dest('dist/demo_batch'));
}

function mainpage(cb) {
  const data = getDataForTextsWithCluster(loadFromDb());
  const rendered = rosaenlg.renderFile('src/demo_batch/all.pug',
    {
      language: 'fr_FR',
      data: data
    });
  fs.writeFile('dist/demo_batch/agrisub.html', rendered, 'utf8', cb);
}

exports.mainpage = mainpage;
exports.all = parallel(css, mainpage);
