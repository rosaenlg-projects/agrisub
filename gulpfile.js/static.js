const { src, dest, parallel, series } = require('gulp');


function static() {
  return src('src/static/*')
    .pipe(dest('dist'));
}

exports.all = series(static);
