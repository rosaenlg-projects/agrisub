const { src, dest, parallel, series } = require('gulp');
const fs = require('fs');
const { exec } = require('child_process');

const s3 = require('./s3');

const demo_dashboard = require('./demo_dashboard');
const demo_excel = require('./demo_excel');
const demo_batch = require('./demo_batch');
const demo_pbi = require('./demo_pbi');

const static = require('./static');

function init(cb) {
  const folders = [
    'dist',
    'dist/demo_batch',
    'dist/demo_excel',
    'dist/demo_dashboard',
    'dist/demo_pbi',
  ];

  folders.forEach(dir => {
      if(!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
          console.log('📁  folder created:', dir);    
      }   
  });
  cb();
}

function clean(cb) {
  exec('rm -rf dist');
  cb();
}

exports.init = init;
exports.clean = clean;

exports.all = series(init, parallel( demo_dashboard.all, demo_excel.all, demo_batch.all, static.all, demo_pbi.all ));

exports.s3 = s3.all;
