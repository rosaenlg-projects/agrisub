# Agrisub

Agrisub is a demo project for RosaeNLG in French language based on public data on agricultural subventions in France.

It features:

* server side batch generation of texts
* front side (browser) generation of text with data coming from a drag and drop on an Excel file
* interactive dashboard using RosaeNLG and d3 (rendering of texts also done in the browser)

